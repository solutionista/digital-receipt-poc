<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'id',
        'store_name',
        'store_tax_id',
        'store_address',
        'store_telefon',
        'bill_number',
        'bill_price_mwst_high',
        'bill_price_mwst_low',
        'bill_price_brutto',
        'bill_price_netto',
        'bill_items'
    ];
}
