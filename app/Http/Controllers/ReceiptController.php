<?php

namespace App\Http\Controllers;

use App\Receipt;
use Illuminate\Http\Request;

class ReceiptController extends Controller
{
    /**
     * Show digital receipt in view
     *
     * @return view
     */
    public function index($receiptId) {
        $receipt = Receipt::where('id', $receiptId)->first();

        if($receipt == null)
            abort(404);

        $receipt->bill_items = json_decode($receipt->bill_items);

        return view('receipt')->with('bill', $receipt);
    }

    /**
     * Create a digital receipt
     *
     * @return json
     */
    public function createReceipt(Request $request) {
        $this->validate($request, [
            "id" => 'required',
            'store_name' => 'required',
            'store_tax_id'=> 'required',
            'store_address' => 'required',
            'store_telefon' => 'required',
            'bill_number' => 'required',
            'bill_price_mwst_high' => 'required',
            'bill_price_mwst_low' => 'required',
            'bill_price_brutto' => 'nullable',
            'bill_price_netto' => 'nullable',
            'bill_items' => 'required'
        ]);

        $input = $request->all();

        Receipt::create([
            'id' => $input['id'],
            'store_name' => $input['store_name'],
            'store_tax_id'=> $input['store_tax_id'],
            'store_address' => $input['store_address'],
            'store_telefon' => $input['store_telefon'],
            'bill_number' => $input['bill_number'],
            'bill_price_mwst_high' => $input['bill_price_mwst_high'],
            'bill_price_mwst_low' => $input['bill_price_mwst_low'],
            'bill_price_brutto' => $input['bill_price_brutto'],
            'bill_price_netto' => $input['bill_price_netto'],
            'bill_items' =>  $input['bill_items']
        ]);

        return response()->json(
            [
                'status' => true
            ]
        );
    }

    /**
     * Get a digital receipt from the database
     *
     * @return json
     */
    public function getReceipt($receiptId) {
        $receipt = Receipt::where('id', $receiptId)->first();

        return response()->json($receipt, 200);
    }
}
