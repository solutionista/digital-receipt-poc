## How to install

Step 1: Clone this git repository (git clone xx)
Step 2: Download and install composer (getcomposer.org)
Step 3: Open a Terminal in the repos root directory and install all dependencies (composer install)
Step 4: Install mysql, create a user and a database -> pass everything into the .env.example file in the repos root directory and rename it to .env
Step 5: Run "php artisan migrate" in the terminal
Step 6: Run "php artisan serve" and follow the shown link, now you are ready :)

![Screenshots](https://i.imgur.com/aDy982e.png)