<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->text('store_name'); // *
            $table->text('store_tax_id'); // *
            $table->text('store_address'); // *
            $table->text('store_telefon'); // * Would be better in seperate table, but data could change so -> change controlled table
            
            $table->text('bill_number');
            $table->double('bill_price_mwst_high')->nullable()->default(0);
            $table->double('bill_price_mwst_low')->nullable()->default(0);
            $table->double('bill_price_brutto');
            $table->double('bill_price_netto');
            $table->text('bill_items');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipt');
    }
}
