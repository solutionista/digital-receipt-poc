<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Receipt</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="Content-Language" content="en" />
        <meta name="Language" content="English, en, english" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />   

        <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet" />
        
        <style>
            html {
                font-family: 'Montserrat', sans-serif;
                background-color: #f4f4f8;
                height: 100%;
            }

            body {
                margin: 0 15px;
                height: 100%;
            }
         
            .container {
                display: flex;
                flex-direction: column;
                justify-content: center;
                justify-items: center;

                height: 100%;

                max-width: 400px;
                margin: 0 auto;
            }

            h3, h4 {
                margin: 0;
                font-weight: normal
            }

            .store {
                text-align: center
            }

            .store h3, .store h4, p {
                margin: 5px;
            }

            .price {
                width: 180px;
                margin-left: auto
            }

            .price div {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
            }

            .options {
                display: flex;
                flex-direction: row;
                justify-content: space-evenly;

                border: 1px solid grey;
                padding: 5px;
                margin-top: 20px;
            }

            hr {
                margin: 20px 0;
            }

            .bill-piece {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="store">
                <h3>{{ $bill->store_name }}</h3>
                <h4>{{ $bill->store_address }}</h4>
                <h4>Tel. {{ $bill->store_telefon }}</h4>
                <h4>Tax Id: {{ $bill->store_tax_id }}</h4>
                <h4>Bill Number: {{ $bill->bill_number }}</h4>
            </div>
            <div class="bill-item">                
                <div class="bill-piece">
                    <h5>Name:</h5>
                    <h5>Price:</h5>
                </div>
                @foreach($bill->bill_items as $items)
                <div class="bill-piece">
                    <p>{{ $items->name }} </p>
                    <p>{{ $items->price }} €</p>
                </div>
               
                @endforeach
            </div>
            <hr/>
            <div class="price">
                <div>
                    <h4>Brutto</h4>
                    <h4>{{ $bill->bill_price_brutto }} €</h4>
                </div>
                @if($bill->bill_price_mwst_high != 0)
                <div>
                    <h4>MwSt. 16%</h4>
                    <h4>{{ $bill->bill_price_mwst_high }} €</h4>
                </div>
                @endif
                @if($bill->bill_price_mwst_low != 0)
                <div>
                    <h4>MwSt. 5%</h4>
                    <h4>{{ $bill->bill_price_mwst_low }} €</h4>
                </div>
                @endif
                <div>
                    <h4>Netto</h4>
                    <h4>{{ $bill->bill_price_netto }} €</h4>
                </div>
            </div>

            <div class="options">
                <div>
                    Share
                </div>
                <div>
                    Delete
                </div>
            </div>
        </div>
    </body>
</html>