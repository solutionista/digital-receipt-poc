<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/receipt', 'ReceiptController@createReceipt');
Route::get('/receipt/{id}', 'ReceiptController@getReceipt');